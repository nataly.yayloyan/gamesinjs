document.addEventListener("DOMContentLoaded", ()=>{


	const cardArray = [
		{
			name: 'fries',
			img: 'images/fries.png'
		},
		{
			name: 'fries',
			img: 'images/fries.png'
		},
		{
			name:"cheeseburger",
			img: 'images/cheeseburger.png'
		},
		{
			name:"cheeseburger",
			img: 'images/cheeseburger.png'
		},
		{
			name: 'hotdog',
			img: 'images/hotdog.png'
		},
		{
			name: 'hotdog',
			img: 'images/hotdog.png'
		},
		{
			name: 'ice-cream',
			img: 'images/ice-cream.png'
		},
		{
			name: 'ice-cream',
			img: 'images/ice-cream.png'
		},
		{
			name: 'milkshake',
			img: 'images/milkshake.png'
		},
		{
			name: 'milkshake',
			img: 'images/milkshake.png'
		},
		{
			name: 'pizza',
			img: 'images/pizza.png'
		},
		{
			name: 'pizza',
			img: 'images/pizza.png'
		},
	];

	cardArray.sort(() => 0.5-Math.random());

	const grid = document.querySelector(".grid");
	const resultDisplay = document.querySelector('#result');
	let cardsChosen=[];
	let cardsChosenID=[];
	let cardsWon = [];

	function createBoard(){
		for(let i=0;i<cardArray.length;i++){
			let card = document.createElement("img");
			card.setAttribute('src', 'images/blank.png');
			card.setAttribute('data-id', i);
			card.addEventListener('click', flipCard);
			grid.appendChild(card)
		}

	}
	//// check match
	function checkForMatch() {
		let cards = document.querySelectorAll('img');
		const optionOneId = cardsChosenID[0];
		const optionTwoId = cardsChosenID[1];
		if (cardsChosen[0]===cardsChosen[1]){
			alert('You found match :)');
			cards[optionOneId].setAttribute('src','images/white.png');
			cards[optionTwoId].setAttribute('src','images/white.png');
			cardsWon.push(cardsChosen)
		}
		else {
			cards[optionOneId].setAttribute('src','images/blank.png');
			cards[optionTwoId].setAttribute('src','images/blank.png');
			alert('Sorry try again')
		}
		cardsChosen = [];
		cardsChosenID = [];
		resultDisplay.textContent = cardsWon.length;
		if(cardsWon.length===cardArray.length/2){
			resultDisplay.textContent = 'Congrats! All found!'
		}
	}

	//// flipcard
	function flipCard(){
		let cardID = this.getAttribute('data-id');
		cardsChosen.push(cardArray[cardID].name);
		cardsChosenID.push(cardID);
		this.setAttribute('src', cardArray[cardID].img);
		if(cardsChosen.length===2){
			setTimeout(checkForMatch,500)
		}
	}

	createBoard()

}
)